
// config file for pm2, production version: `pm2 start ecosystem.config.js`

module.exports = {
   apps : [
      {
         name: "foodtroc",
         script: "./src/index.js",
         watch: false,
         instance_var: 'INSTANCE_ID', // see: https://pm2.keymetrics.io/docs/usage/environment/#node_app_instance-pm2-25-minimum

         env: {
            "PORT": 5050,
            "NODE_ENV": "production",
         },
      }
   ]
}

