//contient les services d'accès aux pages

const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const service = require('feathers-knex')
const knex = require('knex') //permet d'acceder à la base de données
const socketio = require('@feathersjs/socketio') // permet de configurer les websocket pas utile ici car on prend http
const path = require('path') // permet de se deplacer dans l'arborescence des fichiers (voir app.set views)
let config = require('./knexfile.js') //donne accès aux caracteristiques de la bdd
let database = knex(config.development) //definit l'état de la bdd (dvpt production etc...)



// Create a feathers instance.
const app = express(feathers())

// Content-Type: application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }))

// Content-Type: application/json
app.use(express.json())

// Content-Type: html/text
app.use(express.text())

//indique la localisation des pages (dans le dossier views, __dirname pour indique que c'est le meme repertoire)
app.set("views", path.join(__dirname, "views"))



// Transport: HTTP & websocket
//app.configure(express.rest())
app.configure(socketio()) // il faut choisir un des deux, ici on a pris websocket

// Custom services
app.configure(mailService)

// Feathers REST services
let categoryService = service({Model: database, name: 'Category'})
let subcategoryService = service({Model: database, name: 'Subcategory'})
let produitService = service({Model: database, name: 'Produit'})
let annonceService = service({Model: database, name: 'Annonce'})
let iconeService = service({Model: database, name: 'Icone'})

app.use('/api/category', categoryService)
app.use('/api/subcategory', subcategoryService)
app.use('/api/produit', produitService)
app.use('/api/annonce', annonceService)
app.use('/api/icone', iconeService)

//creation BDD
async function createBDD() {
   try {
      let db = knex(config.development)

      let categoryTableExists = await db.schema.hasTable('Category')
      if (categoryTableExists) {
         console.log('table "Category" already exists')
      } else {
         await db.schema.createTable('Category', table => {
            table.increments('id')
            table.string('name').unique().notNull()
            table.timestamp('created_at').defaultTo(db.fn.now()) // 2020-12-02T08:52:00Z
         })
      }

      let subcategoryTableExists = await db.schema.hasTable('Subcategory')
      if (subcategoryTableExists) {
         console.log('table "Subcategory" already exists')
      } else {
         await db.schema.createTable('Subcategory', table => {
            table.increments('id')
            table.string('name').unique().notNull()
            table.foreign('category').references('name').inTable('Category');
            table.timestamp('created_at').defaultTo(db.fn.now()) // 2020-12-02T08:52:00Z
         })
      }

      let produitTableExists = await db.schema.hasTable('Produit')
      if (produitTableExists) {
         console.log('table "Produit" already exists')
      } else {
         await db.schema.createTable('Produit', table => {
            table.increments('id')
            table.string('name').unique().notNull()
            table.string('description')
            table.foreign('subcategory').references('name').inTable('Subcategory');
            table.timestamp('created_at').defaultTo(db.fn.now()) // 2020-12-02T08:52:00Z
         })
      }

      let annonceTableExists = await db.schema.hasTable('Annonce')
      if (annonceTableExists) {
         console.log('table "Annonce" already exists')
      } else {
         await db.schema.createTable('Annonce', table => {
            table.increments('id')
            table.string('localisation')
            table.string('status')
            table.string('description')
            table.float('price')
            table.integer('quantity')
            table.foreign('product').references('name').inTable('Product');
            table.timestamp('created_at').defaultTo(db.fn.now()) // 2020-12-02T08:52:00Z
         })
      }

      let iconeTableExists = await db.schema.hasTable('Icone')
      if (iconeTableExists) {
         console.log('table "Icone" already exists')
      } else {
         await db.schema.createTable('Icone', table => {
            table.increments('id')
            table.string('coordinates')
            table.string('imageUrl')
            table.foreign('annonce_id').references('id').inTable('Annonce');
            table.timestamp('created_at').defaultTo(db.fn.now()) // 2020-12-02T08:52:00Z
         })
      }



   } catch(err) {
      console.log(err.toString())
   }
}
createBDD()

// Start the server.
const port = process.env.PORT || 3000

app.listen(port, async () => {
  console.log(`Server listening on port ${port}`)
})
