const { Service } = require('feathers-knex');

exports.Annonce = class Annonce extends Service {
  constructor(options) {
    super({
      ...options,
      name: 'annonce'
    });
  }
};
