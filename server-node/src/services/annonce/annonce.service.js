// Initializes the `users` service on path `/users`
const { Annonce } = require('./annonce.class');
const createModel = require('../../models/annonce.model');
const hooks = require('./annonce.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/annonce', new Annonce(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('annonce');

  service.hooks(hooks);
};
