const { Service } = require('feathers-knex');

exports.Product = class Product extends Service {
  constructor(options) {
    super({
      ...options,
      name: 'product'
    });
  }
};
