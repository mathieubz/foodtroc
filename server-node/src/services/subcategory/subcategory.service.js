// Initializes the `subcategory` service on path `/subcategory`
const { Subcategory } = require('./subcategory.class');
const createModel = require('../../models/subcategory.model');
const hooks = require('./subcategory.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/subcategory', new Subcategory(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('subcategory');

  service.hooks(hooks);
};
