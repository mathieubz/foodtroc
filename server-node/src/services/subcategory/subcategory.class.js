const { Service } = require('feathers-knex');
exports.Subcategory = class Subcategory extends Service {
  constructor(options) {
    super({
      ...options,
      name: 'subcategory'
    });
  }
};
