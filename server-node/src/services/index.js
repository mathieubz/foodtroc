const users = require('./users/users.service.js');
const annonce = require('./annonce/annonce.service.js');
const category = require('./category/category.service.js');
const icone = require('./icone/icone.service.js');
const product = require('./product/product.service.js');
const subcategory = require('./subcategory/subcategory.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(category);
  app.configure(subcategory);
  app.configure(product);
  app.configure(annonce);
  app.configure(icone);
};
