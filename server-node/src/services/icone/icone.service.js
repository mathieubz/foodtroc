// Initializes the `icone` service on path `/icone`
const { Icone } = require('./icone.class');
const createModel = require('../../models/icone.model');
const hooks = require('./icone.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/icone', new Icone(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('icone');

  service.hooks(hooks);
};
