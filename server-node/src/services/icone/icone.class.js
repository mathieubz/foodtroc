const { Service } = require('feathers-knex');

exports.Icone = class Icone extends Service {
  constructor(options) {
    super({
      ...options,
      name: 'icone'
    });
  }
};
