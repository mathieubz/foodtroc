from django.urls import path, include
from rest_framework import routers

from map import views
from map.views import AnnonceViewSet, ProduitViewSet, SubcategoryViewSet, CategoryViewSet,IconeViewSet, Login, UserViewSet

router = routers.DefaultRouter()
router.register('map/annonce', AnnonceViewSet)
router.register('map/produit', ProduitViewSet)
router.register('map/subcategory', SubcategoryViewSet)
router.register('map/category', CategoryViewSet)
router.register('map/icone', IconeViewSet)
router.register('map/user', UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('map/auth', Login.as_view(), name='Login')
]
