from rest_framework import filters
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist

from django.contrib.auth.models import User
from map.models import Annonce, Produit, Subcategory, Category, Icone
from map.serializers import AnnonceSerializer, ProduitSerializer, SubcategorySerializer, CategorySerializer, IconeSerializer, UserSerializer

class AnnonceViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Annonce.objects.all()
    serializer_class = AnnonceSerializer
    filter_backends = [filters.SearchFilter]
    filterset_fields = ['localisation', 'product', 'product__subcategory','product__subcategory__category']
    search_fields = ['localisation', 'product__name', 'product__subcategory__name', 'product__subcategory__category__name','description']

class ProduitViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Produit.objects.all()
    serializer_class = ProduitSerializer

class IconeViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Icone.objects.all()
    serializer_class = IconeSerializer

class SubcategoryViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Subcategory.objects.all()
    serializer_class = SubcategorySerializer

class CategoryViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class Login(APIView):

    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        if not request.data:
            return Response({'Error': "Please provide username/password"}, status="400")
        
        username = request.data['username']
        password = request.data['password']
        print('Username', username, 'Password', password)
        try:
            user = User.objects.get(username=username)
        except ObjectDoesNotExist:
            return Response("Invalid credentials", status=400)
        except Exception as e:
            print(e)
            return Response("Unknown server error", status=500)
        if user and user.check_password(password):
            return Response({
                'username': user.username,
                'password': user.password
                }, status=200)
        else:
            return Response("Invalid credentials", status=400)

class UserViewSet(ModelViewSet):

    permission_classes = (AllowAny,)
    
    queryset = User.objects.all()
    serializer_class = UserSerializer