from django.contrib import admin
from map.models import Annonce, Produit, Subcategory, Category, Icone

class AnnonceAdmin(admin.ModelAdmin):
    list_display = (
        'status',
        'localisation',
        'price',
        'description',
        'quantity',
        'product',
    )

class ProduitAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
        'subcategory',
    )

class IconeAdmin(admin.ModelAdmin):
    list_display = (
        'coordinates',
        'imageUrl',
        'annonce_id',
    )

class SubcategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'category',
    )

class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
    )

admin.site.register(Annonce, AnnonceAdmin)
admin.site.register(Produit, ProduitAdmin)
admin.site.register(Subcategory, SubcategoryAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Icone, IconeAdmin)