from django.db import models
from django.contrib.auth.models import AbstractUser

class Category(models.Model):
    name = models.CharField(max_length=30,unique=True,default='')
    def __str__(self):
        return self.name

class Subcategory(models.Model):
    name = models.CharField(max_length=30,unique=True,default='')
    category = models.ForeignKey(Category, on_delete=models.CASCADE,default=None)
    def __str__(self):
        return self.name

class Produit(models.Model):
    name = models.CharField(max_length=30,default='')
    description = models.TextField()
    subcategory = models.ForeignKey(Subcategory, on_delete=models.CASCADE,default=None)
    def __str__(self):
        return self.name
    

class Annonce(models.Model):
    status = models.CharField(max_length=30)
    localisation = models.CharField(max_length=50)
    price = models.FloatField()
    description = models.TextField()
    quantity = models.CharField(max_length=30)
    product = models.ForeignKey(Produit, on_delete=models.CASCADE,default=None)
    def __str__(self):
        return self.localisation

class Icone(models.Model):
    coordinates = models.TextField()
    imageUrl = models.TextField()
    annonce_id = models.ForeignKey(Annonce, on_delete=models.CASCADE,default=None)
    def __str__(self):
        return self.imageUrl
