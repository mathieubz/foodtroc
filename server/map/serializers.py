from rest_framework import serializers
from map.models import Annonce, Produit, Subcategory, Category, Icone
from django.contrib.auth.models import User

class AnnonceSerializer(serializers.ModelSerializer):
    product = serializers.SlugRelatedField(
        read_only=True,
        slug_field='name'
    )

    class Meta:
        model = Annonce
        fields = "__all__"

class ProduitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Produit
        fields = "__all__"

class IconeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Icone
        fields = "__all__"

class SubcategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Subcategory
        fields = "__all__"

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"
    
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model =  User
        fields = "__all__"