# Generated by Django 3.1.5 on 2021-02-02 14:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0002_auto_20210113_1326'),
    ]

    operations = [
        migrations.CreateModel(
            name='Icone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('imageUrl', models.FloatField()),
                ('annonce_id', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='map.annonce')),
            ],
        ),
    ]
