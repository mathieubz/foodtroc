import './App.css';
import React from 'react';

import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";

import Home from "./Views/Home";
import Register from "./Views/Register";
import Login from "./Views/Login";
import UserProfil from "./Views/UserProfil"
import Header from "./Components/Header"

function App() {
  return (
    <HashRouter>
    <div>
      <Header />
      <br></br>
      <div className="content">
        <Route exact path="/" component={Home}/>
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
        <Route path='/user-profil' component={UserProfil}/>
      </div>
    </div>
  </HashRouter>
  );
}

export default App;
