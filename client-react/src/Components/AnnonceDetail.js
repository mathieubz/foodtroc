import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

class AnnonceDetail extends React.Component {
    constructor(props) {
        super(props);
    }

    render () {
        return (
            <div className="card">
                <h1>Detail de l'annonce :</h1>
                <div className="col-md-12">
                    <figure>
                        <div className="img-wrap"><img src="https://thumbs.dreamstime.com/b/corbeille-de-fruits-59780737.jpg" /></div>
                        <p>{this.props.annonce.status}</p>
                        <figcaption className="info-wrap">
                            <h4 className="title">{this.props.annonce.product}</h4>
                            <p className="desc">{this.props.annonce.description}</p>
                            <div className="rating-wrap">
                                <div className="label-rating">{this.props.annonce.localisation}</div>
                                <div className="label-rating">{this.props.annonce.category} </div>
                                <div className="label-rating">{this.props.annonce.quantity}</div>
                                <div className="label-rating"> 17 orders </div>

                            </div>
                        </figcaption>
                        <div className="bottom-wrap">
                            <a href="" className="btn btn-sm btn-primary float-right">Order Now</a>
                            <div className="price-wrap h5">
                                <span className="price-new">${this.props.annonce.price}</span>
                            </div>
                        </div>
                    </figure>
                </div>
            </div>
        )
    }
}

export default AnnonceDetail;
