import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

class Annonces extends React.Component {
    render () {
        return (
            <div className="container card">
                <h1>Annonce :</h1>
                <div className="row">
                    <div className="col-md-10">
                        {this.props.annonces
                            .map((annonce, index) => (
                                <figure
                                    className="card card-product"
                                    onClick={() => this.props.onClickShowDetail(index)}
                                >
                                    <div className="img-wrap"><img src="https://thumbs.dreamstime.com/b/corbeille-de-fruits-59780737.jpg" /></div>
                                    <figcaption className="info-wrap">
                                        <h4 className="title">{annonce.product}</h4>
                                        <p className="desc">Some small description goes here</p>
                                        <div className="rating-wrap">
                                            <div className="label-rating">{annonce.localisation}</div>
                                            <div className="label-rating"> 17 orders </div>
                                        </div>
                                    </figcaption>
                                    <div className="bottom-wrap">
                                        <div className="price-wrap h5">
                                            <span className="price-new">${annonce.price}</span>
                                        </div>
                                    </div>
                                </figure>
                            ))}
                    </div>
                </div>
            </div>
        )
    }
}

export default Annonces;
