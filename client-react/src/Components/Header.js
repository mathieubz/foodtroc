import React from 'react'
import { Link, Switch } from "react-router-dom"; import {
    Route,
    NavLink,
    HashRouter
} from "react-router-dom";


class Header extends React.Component {

    render() {
        return (
            <header>
                <nav>
                    <ul className="flex-container">
                        <li className="item">Food'troc</li>
                        <li className="flex-item"><NavLink className="navlink" to="../">Home</NavLink></li>
                        <li className="flex-item"><NavLink className="navlink" to="/register">Register</NavLink></li>
                        <li className="flex-item"><NavLink className="navlink" to="/login">Login</NavLink></li>
                        <li className="flex-item"><NavLink className="navlink" to="/user-profil">Profil</NavLink></li>
                    </ul>
                </nav>
            </header>


            /*
                        <div className="home-wrapper">
                            <div classNameame="row">
                                <div className="column">
                                    <h1>Food'Troc</h1>
                                </div>
            
                                <div className="column">
                                    <div>
                                        <NavLink to="/">Home</NavLink>
                                        <NavLink to="/register">Register</NavLink>
                                        <NavLink to="/login">Login</NavLink>
                                        <NavLink to="/user-profil">User</NavLink>
                                    </div>
                                </div>
                            </div>
                        </div> */
        )
    }
}

export default Header;