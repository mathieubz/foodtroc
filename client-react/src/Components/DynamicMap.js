import React from 'react'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import {useState, useEffect} from 'react'

function DynamicMap (props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [markers, setMarkers] = useState([]);

    const io = require('socket.io-client');
    const feathers = require('@feathersjs/feathers');
    const socketio = require('@feathersjs/socketio-client');

    const socket = io(process.env.DOMAIN_NAME);
    const app = feathers();

    app.configure(socketio(socket));

    useEffect(() => {
      app.service('icone').find()
        .then(
          (result) => {
              setIsLoaded(true);
              setMarkers(result);
              console.log("ANNONCES :", result);
          },
          (error) => {
              setIsLoaded(true);
              setError(error);
          }
        )
    }, [])

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        
        return (
            <div>
                <MapContainer center={[43.604652, 1.444209]} zoom={13} scrollWheelZoom={false}>
                    <TileLayer
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />

                    {markers.map((item, index) => (
                      
                      <Marker 
                        position={item.coordinates.split(',')}   
                        eventHandlers={{
                          click: (e) => {
                            console.log('marker clicked', e)
                            props.onClickIcone(index)
                          },
                        }}>
                          <Popup>
                              {item.id}        
                          </Popup>
                      </Marker>
                    ))}
                </MapContainer> 
            </div>
        );
    }
}

export default DynamicMap
