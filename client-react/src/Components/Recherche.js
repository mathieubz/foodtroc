import React from 'react'
import {useState, useEffect} from 'react'

function Recherche(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    const io = require('socket.io-client');
    const feathers = require('@feathersjs/feathers');
    const socketio = require('@feathersjs/socketio-client');

    const socket = io(process.env.DOMAIN_NAME)
    const app = feathers();

    app.configure(socketio(socket));

    useEffect(() => {
        app.service('annonce').find()
            .then(
            (result) => {
                setIsLoaded(true);
                setItems(result);
                props.onClickListeAnnonce(result);
                console.log("ANNONCES :", result);
            },
            (error) => {
                setIsLoaded(true);
                setError(error);
            }
        )
    }, [])

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <div></div>
        );
    }
}

export default Recherche
