import React, { Component } from "react";
import Annonces from '../Components/Annonces'
import Recherche from '../Components/Recherche'
import DynamicMap from '../Components/DynamicMap'
import AnnonceDetail from '../Components/AnnonceDetail'

import './Home.css'

class Home extends Component {
  constructor() {
    super();
    this.state = {
      annonces: [],
      annonceDetail: {},
      iconeIndex: null
    };
    this.onClickListeAnnonce = this.onClickListeAnnonce.bind(this);
    this.onClickShowDetail = this.onClickShowDetail.bind(this);
  }

  onClickListeAnnonce(listeAnnonces) {
    this.setState({ annonces: listeAnnonces })
  }

  onClickShowDetail(annonceId) {
    console.log("Annonce Detail", annonceId)
    this.setState({ annonceDetail: this.state.annonces[annonceId] })
  }

  render() {
    return (
      <div className="content">
        <div id='mapid'>
          <DynamicMap onClickIcone={this.onClickShowDetail} />
        </div>
        <br></br>
        <Recherche onClickListeAnnonce={this.onClickListeAnnonce} />
        <br></br>
        <div className="home-wrapper">
          <div className='row'>
            <div class='column'>
              <AnnonceDetail annonce={this.state.annonceDetail} />
            </div>
            <div className='column'>
              <Annonces annonces={this.state.annonces} onClickShowDetail={this.onClickShowDetail} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;