import React, { Component } from "react";
 
class Contact extends Component {
  render() {
    return (
      <div className="wrapper fadeInDown">
        <div id="formContent">
          <h1> Login : </h1>
          <form className="login">
            <input v-model="username" type="text" id="login" className="fadeIn second" name="login" placeholder="login" />
            <input v-model="password" type="password" id="password" className="fadeIn third" name="login" placeholder="password" />
            <input type="submit" className="fadeIn fourth" value="Log In" />
          </form>
    
          <div id="formFooter">
            <a className="underlineHover" href="#">Register</a> |
            <a className="underlineHover" href="#">Forgot Password?</a>
          </div>
    
        </div>
      </div>
    );
  }
}
 
export default Contact;