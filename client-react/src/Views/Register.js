import React, { Component } from "react";
 
class Stuff extends Component {
  render() {
    return (
      <div className="content">
        <link rel="stylesheet" href= "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"></link>
        <h1> Register </h1>
            <div className="row main">
              <div className="main-login main-center">
                <form className="form-horizontal" method="post" action="#">

                  <div className="form-group">
                    <label for="lastname" className="cols-sm-2 control-label">Lastname</label>
                    <div className="cols-sm-10">
                      <div className="input-group">
                        <span className="input-group-addon"><i className="fa fa-user fa" aria-hidden="true"></i></span>
                        <input type="text" className="form-control" name="lastname" id="lastname"  placeholder="Enter your lastname"/>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <label for="firstname" className="cols-sm-2 control-label">Firstname</label>
                    <div className="cols-sm-10">
                      <div className="input-group">
                        <span className="input-group-addon"><i className="fa fa-user fa" aria-hidden="true"></i></span>
                        <input type="text" className="form-control" name="firstname" id="firstname"  placeholder="Enter your firstname"/>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <label for="email" className="cols-sm-2 control-label">Your Email</label>
                    <div className="cols-sm-10">
                      <div className="input-group">
                        <span className="input-group-addon"><i className="fa fa-envelope fa" aria-hidden="true"></i></span>
                        <input type="text" className="form-control" name="email" id="email"  placeholder="Enter your Email"/>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <label for="username" className="cols-sm-2 control-label">Username</label>
                    <div className="cols-sm-10">
                      <div className="input-group">
                        <span className="input-group-addon"><i className="fa fa-users fa" aria-hidden="true"></i></span>
                        <input type="text" className="form-control" name="username" id="username"  placeholder="Enter your Username"/>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <label for="password" className="cols-sm-2 control-label">Password</label>
                    <div className="cols-sm-10">
                      <div className="input-group">
                        <span className="input-group-addon"><i className="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                        <input type="password" className="form-control" name="password" id="password"  placeholder="Enter your Password"/>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <label for="confirm" className="cols-sm-2 control-label">Confirm Password</label>
                    <div className="cols-sm-10">
                      <div className="input-group">
                        <span className="input-group-addon"><i className="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                        <input type="password" className="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
                      </div>
                    </div>
                  </div>
                  <br />
                  <div className = "button-register">
                    <button type="button" className="btn btn-primary btn-lg btn-block login-button">Register</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
    );
  }
}
 
export default Stuff;