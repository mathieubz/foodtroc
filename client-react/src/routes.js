import React from 'react';

const Home = React.lazy(() => import('./Views/Home'));

const routes = [
    {path: '/', name: 'Home', component: Home},
]