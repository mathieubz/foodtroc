import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loginUser: undefined
  },

  mutations: {
    AUTH_LOGIN (state, user) {
      state.loginUser = user
      console.log('AUTH_LOGIN', state.loginUser)
    },

    AUTH_LOGOUT (state) {
      state.loginUser = undefined
      console.log('AUTH_LOGOUT', state.loginUser)
    }
  },

  getters: {
    isLoggedIn: state => !!state.loginUser
  }
})
