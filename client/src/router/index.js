import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '../store.js'

import Home from '../views/Home.vue'
import AjoutAnnonce from '../views/AjoutAnnonce.vue'
import Login from '../views/Login.vue'
import MyProfil from '../views/MyProfil.vue'
import Register from '../views/Register.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/add',
    name: 'AjoutAnnonce',
    component: AjoutAnnonce,
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/my-profil',
    name: 'My Profil',
    component: MyProfil,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router
